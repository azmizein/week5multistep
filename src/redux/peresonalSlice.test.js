import { configureStore } from '@reduxjs/toolkit';
import personalReducer, {
  setName,
  setEmail,
  setAddOns,
  selectPlan,
  setIsAnnualPlan,
  setPhoneNumber,
} from './personalSlice';

describe('personalSlice reducers', () => {
  let store;

  beforeEach(() => {
    store = configureStore({ reducer: { personal: personalReducer } });
  });

  it('should set the name', () => {
    const testName = 'John Doe';
    store.dispatch(setName(testName));

    const state = store.getState().personal;
    expect(state.name).toBe(testName);
  });

  it('should set the email', () => {
    const testEmail = 'test@example.com';
    store.dispatch(setEmail(testEmail));

    const state = store.getState().personal;
    expect(state.email).toBe(testEmail);
  });

  it('should set the number', () => {
    const initialState = { phoneNumber: '' };
    const newState = personalReducer(initialState, setPhoneNumber('123-456-7890'));
    expect(newState.someOtherField).toEqual(initialState.someOtherField);
  });

  it('should handle setAddOns', () => {
    const initialState = { addons: { onlineservice: { isAdded: false } } };
    const newState = personalReducer(initialState, setAddOns({ title: 'onlineservice', isChecked: true }));
    expect(newState.addons.onlineservice.isAdded).toBe(true);
  });

  it('should handle selectPlan', () => {
    const initialState = { selectedPlan: null };
    const plan = { name: 'arcade', price: 9, annual: false };
    const newState = personalReducer(initialState, selectPlan(plan));
    expect(newState.selectedPlan).toEqual(plan);
  });

  it('should handle setIsAnnualPlan', () => {
    const initialState = { isAnnualPlan: false };
    const newState = personalReducer(initialState, setIsAnnualPlan(true));
    expect(newState.isAnnualPlan).toBe(true);
  });
});
