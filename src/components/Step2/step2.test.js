import Step2 from '.';
import { render, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import i18n from '@components/i18n';
import { I18nextProvider } from 'react-i18next';
import configureStore from 'redux-mock-store';

const mockStore = configureStore([]);

describe('Component Step 2', () => {
  test('renders step 2 when theme light', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <Step2 theme="light" />
        </I18nextProvider>
      </Provider>
    );
    const step2Component = getByTestId('step2');
    expect(step2Component).toBeInTheDocument();
  });
  test('dispatches selectPlan action with the correct payload when a plan is clicked', () => {
    const plans = {
      plan1: {
        name: 'Plan 1',
        monthlyPrice: 10,
        yearlyPrice: 100,
      },
      plan2: {
        name: 'Plan 2',
        monthlyPrice: 15,
        yearlyPrice: 150,
      },
    };
    const billingCycle = 'monthly';

    const initialState = {
      personal: {
        plans,
        isAnnualPlan: false,
        selectedPlan: null,
      },
    };

    const store = mockStore(initialState);
  });
});
